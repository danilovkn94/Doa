package ru.sut.doa.model.data.ghr;


public class LhsCompany {

    private int id;

    private String name;

    private String address;

    private MpaCompany mpaCompany;

    public LhsCompany() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public MpaCompany getMpaCompany() {
        return mpaCompany;
    }

    public void setMpaCompany(MpaCompany mpaCompany) {
        this.mpaCompany = mpaCompany;
    }
}
