package ru.sut.doa.model.data.ghr;


public class MpaCompany {

    private int id;

    private String companyName;

    public MpaCompany(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
