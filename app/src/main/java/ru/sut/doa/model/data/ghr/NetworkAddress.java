package ru.sut.doa.model.data.ghr;


public class NetworkAddress {
    private int id;

    private String host;

    private int port;

    private LhsCompany lhsCompany;

    private String preffix;

    public NetworkAddress() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public LhsCompany getLhsCompany() {
        return lhsCompany;
    }

    public void setLhsCompany(LhsCompany lhsCompany) {
        this.lhsCompany = lhsCompany;
    }

    public String getPreffix() {
        return preffix;
    }

    public void setPreffix(String preffix) {
        this.preffix = preffix;
    }
}
