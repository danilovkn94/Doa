package ru.sut.doa;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import ru.sut.doa.model.Doi;
import ru.sut.doa.model.data.ghr.NetworkAddress;
import ru.sut.doa.model.data.lhs.Consignment;

public class MainActivity extends AppCompatActivity{

    private Doi doi;
    private TextView statusTextView;
    private TextView infoTextView;
    private ImageView imageView;

    private String host;
    private String port;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnScan = (Button) findViewById(R.id.btnScan);
        statusTextView = (TextView) findViewById(R.id.status);
        infoTextView = (TextView) findViewById(R.id.resultInfo);
        imageView = (ImageView) findViewById(R.id.photo);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                statusTextView.setText(getResources().getString(R.string.status));
                infoTextView.setText(getResources().getString(R.string.info));

                Intent intent = new Intent(MainActivity.this, QrScanActivity.class);
                startActivityForResult(intent, 1);
            }
        };

        btnScan.setOnClickListener(onClickListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null){
            updateDoi(data.getStringExtra("doi"));
            sendRequestToGhr();
        }
    }

    private void updateDoi(String identificator){
        try {
            doi = new Doi(identificator);
        } catch (Exception ignore) { }

        infoTextView.append("\n    DOI: " + doi.getIdentificator());
    }

    private void sendRequestToGhr() {
        RequestToGhrTask requestToGhrTask = new RequestToGhrTask();
        requestToGhrTask.execute(doi);
    }

    private void sendRequestToLhs(){
        RequestToLhsTask requestToLhsTask = new RequestToLhsTask();
        requestToLhsTask.execute(host, port, doi.getSuffix());
    }

    private void updateStatus(String status){
        statusTextView.append("\n    " + status);
    }

    private class RequestToGhrTask extends AsyncTask<Doi, String, String> {

        @Override
        protected String doInBackground(Doi... doi) {

            publishProgress("Sending request to GHR...");

            String url = "http://95.46.8.137:8090/rest/" + doi[0].getPreffix() + "/";
            //String url = "http://192.168.0.10:8090/rest/" + doi[0].getPreffix() + "/";
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            NetworkAddress networkAddress = restTemplate.getForObject(url, NetworkAddress.class);

            publishProgress("Response from GRH is received!");

            host = networkAddress.getHost();
            port = String.valueOf(networkAddress.getPort());

            String info = "\n    Host: " + host +
                    "\n    Port: " + port +
                    "\n    Company: " + networkAddress.getLhsCompany().getName();

            return info;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            updateStatus(values[0]);
        }

        protected void onPostExecute(String result) {
            infoTextView.append(result);

            sendRequestToLhs();
        }
    }


    private class RequestToLhsTask extends AsyncTask<String, String, Consignment>{



        @Override
        protected Consignment doInBackground(String... params) {

            publishProgress("Sending request to LHS...");

            String url = "http://" + params[0] + ":" + params[1] + "/rest/" + params[2] + "/";
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            Consignment consignment = restTemplate.getForObject(url, Consignment.class);

            publishProgress("Response from LHS is received!");

            return consignment;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            updateStatus(values[0]);
        }

        protected void onPostExecute(Consignment consignment){

            String info = "\n    Product: " + consignment.getProduct().getName() +
                    "\n    Manufacture date: " + consignment.getManufactureDate() +
                    "\n    Manufacture address: " + consignment.getManufacture().getAddress() +
                    "\n    Product info: " + consignment.getProduct().getInfo();

            byte[] photo = consignment.getProduct().getPhoto();

            Bitmap bitmap = BitmapFactory.decodeByteArray(photo, 0, photo.length);

            infoTextView.append(info);
            imageView.setImageBitmap(bitmap);
        }
    }

}
